#include "mogl.h"

#include <iostream>
#include <math.h>
#include <memory>


namespace mogl
{
	struct Box
	{
		glm::mat4 model = glm::mat4( 1.f );
		float vert[180] = {
		    -.5f, -.5f, -.5f,  0.f, 0.f,
		     .5f, -.5f, -.5f,  1.f, 0.f,
		     .5f,  .5f, -.5f,  1.f, 1.f,
		     .5f,  .5f, -.5f,  1.f, 1.f,
		    -.5f,  .5f, -.5f,  0.f, 1.f,
		    -.5f, -.5f, -.5f,  0.f, 0.f,

			 -.5f, -.5f,  .5f,  0.f, 0.f,
		     .5f, -.5f,  .5f,  1.f, 0.f,
		     .5f,  .5f,  .5f,  1.f, 1.f,
		     .5f,  .5f,  .5f,  1.f, 1.f,
		    -.5f,  .5f,  .5f,  0.f, 1.f,
		    -.5f, -.5f,  .5f,  0.f, 0.f,

		    -.5f,  .5f,  .5f,  1.f, 0.f,
		    -.5f,  .5f, -.5f,  1.f, 1.f,
		    -.5f, -.5f, -.5f,  0.f, 1.f,
		    -.5f, -.5f, -.5f,  0.f, 1.f,
		    -.5f, -.5f,  .5f,  0.f, 0.f,
		    -.5f,  .5f,  .5f,  1.f, 0.f,

		     .5f,  .5f,  .5f,  1.f, 0.f,
		     .5f,  .5f, -.5f,  1.f, 1.f,
		     .5f, -.5f, -.5f,  0.f, 1.f,
		     .5f, -.5f, -.5f,  0.f, 1.f,
		     .5f, -.5f,  .5f,  0.f, 0.f,
		     .5f,  .5f,  .5f,  1.f, 0.f,

		    -.5f, -.5f, -.5f,  0.f, 1.f,
		     .5f, -.5f, -.5f,  1.f, 1.f,
		     .5f, -.5f,  .5f,  1.f, 0.f,
		     .5f, -.5f,  .5f,  1.f, 0.f,
		    -.5f, -.5f,  .5f,  0.f, 0.f,
		    -.5f, -.5f, -.5f,  0.f, 1.f,

		    -.5f,  .5f, -.5f,  0.f, 1.f,
		     .5f,  .5f, -.5f,  1.f, 1.f,
		     .5f,  .5f,  .5f,  1.f, 0.f,
		     .5f,  .5f,  .5f,  1.f, 0.f,
		    -.5f,  .5f,  .5f,  0.f, 0.f,
		    -.5f,  .5f, -.5f,  0.f, 1.f
		};

		void setVBO( myVBO& vbo )
		{
			vbo.loadToFor( vert, GL_ARRAY_BUFFER, GL_STATIC_DRAW );
			vertAttribPtr<float>::set( 0, 3, false, 5, 0 );
			vertAttribPtr<float>::set( 1, 2, false, 5, 3 );
		}
		void setPos( const glm::vec3& pos, float a, const glm::vec3& axis )
		{
			model = glm::mat4( 1.f );
			model = glm::rotate(model, a, pos );
			model = glm::translate( model, axis );
		}
		void drawByFromAs( myShProgram& program, myVAO& vao, const char* name )
		{
			program.uniSetM4x4v( name, 1, glm::value_ptr(model), false );
			vao.bind(  );
			mogl::draw::arrays( GL_TRIANGLES, 0, 36 );
		}
	};
}


int main(  )
{
	setlocale( LC_ALL, "Russian" );
	GLFWwindow* window = nullptr;
	int width = 800, height = 600;
	mogl::getWindow( window, width, height, "Работа по OpenGL" );
	mogl::view::setPort( window, width, height, true );

	glfwSetKeyCallback( window, mogl::key_callback );

	mogl::Box box;

	mogl::defTextureSettings(  );
	mogl::myTexture texture( "img.jpg" );
	mogl::myTexture texture2( "img2.jpg" );


	mogl::myShProgram program;
	if ( defShProgram( program, "a.vs", "a.frag" ) ) return -1;


	mogl::myVAO vao;
	mogl::myVBO vbo;//, ebo;
	vao.bind(  );
	{
		box.setVBO( vbo );
	}
	mogl::myVAO::unbindAll(  );

	// Camera lesson
	glm::vec3 cPos = glm::vec3( 0.f, 0.f,-3.f );
	glm::vec3 cTarget = glm::vec3( 0.f, 0.f, 0.f );
	glm::vec3 cDirectoin = glm::normalize( cPos - cTarget );
	glm::mat4 view(1.f);
	view = glm::translate(view, glm::vec3(0.0f, 0.0f, -3.0f));

	glm::mat4 proj = mogl::view::set3D( width, height, 100 );
	glm::vec3 boxP[] = {
		glm::vec3( 0.0f,  0.0f,  0.0f),
		glm::vec3( 2.0f,  5.0f, -15.0f),
		glm::vec3(-1.5f, -2.2f, -2.5f),
		glm::vec3(-3.8f, -2.0f, -12.3f),
		glm::vec3( 2.4f, -0.4f, -3.5f),
		glm::vec3(-1.7f,  3.0f, -7.5f),
		glm::vec3( 1.3f, -2.0f, -2.5f),
		glm::vec3( 1.5f,  2.0f, -2.5f),
		glm::vec3( 1.5f,  0.2f, -1.5f),
		glm::vec3(-1.3f,  1.0f, -1.5f)
	};

	// Основной цикл
	while ( !glfwWindowShouldClose( window ) )
	{
		if ( mogl::Keyboard::key::F ) std::cout << "F" << std::endl;
		if ( mogl::Keyboard::key::O ) std::cout << "O" << std::endl;
		glfwPollEvents(  );
		glClearColor( .15f, .15f, .15f, 1.f );
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
			program.use(  );
			texture.bindToBlock( 0, "ourTexture1", program );
			texture2.bindToBlock( 1, "ourTexture2", program );
			program.uniSetM4x4v( "view", 1, glm::value_ptr(view), false );
			program.uniSetM4x4v( "projection", 1, glm::value_ptr( proj ), false );
			for ( int i = 0; i < 10; ++i )
			{
				box.setPos( boxP[i], glm::radians( 20.f * i ), {1.f,.3f,.5f} );
				box.drawByFromAs( program, vao, "model" );
			}
		glfwSwapBuffers( window );
	}
	glfwTerminate();

	return 0;
}
