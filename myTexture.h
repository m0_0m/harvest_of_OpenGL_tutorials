#ifndef MYTEXTURE_H
#define MYTEXTURE_H

#include "myShader.h"
#include "glew_glfw_glm_soil.h"

namespace mogl
{
	class myTexture
	{
	public:
		int width;
		int height;
		GLuint id;
		myTexture( const char* path, bool useAlpha = false )
		{
			glGenTextures( 1, &( this->id ) );
			this->bind(  );
			unsigned char* image = SOIL_load_image(
						path,
						&( this->width ),
						&( this->height ),
						0,
						( useAlpha ? SOIL_LOAD_RGBA : SOIL_LOAD_RGB ) );

			glTexImage2D(
				GL_TEXTURE_2D,
				0,
				( useAlpha ? GL_RGBA : GL_RGB ),
				this->width,
				this->height,
				0,
				( useAlpha ? GL_RGBA : GL_RGB ),
				GL_UNSIGNED_BYTE,
				image );

			glGenerateMipmap( GL_TEXTURE_2D );
			SOIL_free_image_data( image );
		}

		void bindToBlock( GLuint index, const char* name, myShProgram& program )
		{
			glActiveTexture( GL_TEXTURE0 + index );
			this->bind(  );
			glUniform1i( glGetUniformLocation( program.id, name ), index );
		}

		void bind(  )
		{
			glBindTexture( GL_TEXTURE_2D, this->id );
		}

		static void unbindAll(  )
		{
			glBindTexture( GL_TEXTURE_2D, 0 );
		}

	// Интерфейсы для глобальной настройки отрисовки текстур
	private:
		class WrapType
		{
		private:
			GLenum pname;
		public:
			WrapType( GLenum n ) : pname( n ) {}
			void repeat(  )
			{
				glTexParameteri( GL_TEXTURE_2D, pname, GL_REPEAT );
			}

			void mirrored_repeat(  )
			{
				glTexParameteri( GL_TEXTURE_2D, pname, GL_MIRRORED_REPEAT );
			}

			void clamp_to_edge(  )
			{
				glTexParameteri( GL_TEXTURE_2D, pname, GL_CLAMP_TO_EDGE );
			}

			void clamp_to_border(  )
			{
				glTexParameteri( GL_TEXTURE_2D, pname, GL_CLAMP_TO_BORDER );
			}

			void clamp_to_border( float* borderColor )
			{
				glTexParameterfv(
					GL_TEXTURE_2D,
					GL_TEXTURE_BORDER_COLOR,
					borderColor );

				glTexParameteri( GL_TEXTURE_2D, pname, GL_CLAMP_TO_BORDER );
			}
			void clamp_to_border( float r, float g, float b, float a )
			{
				float borderColor[] = { r, g, b, a };
				glTexParameterfv(
					GL_TEXTURE_2D,
					GL_TEXTURE_BORDER_COLOR,
					borderColor );

				glTexParameteri( GL_TEXTURE_2D, pname, GL_CLAMP_TO_BORDER );
			}
		};
		struct MinFilter {
			void setLinear_linearMipmap(  )
			{
				glTexParameteri(
					GL_TEXTURE_2D,
					GL_TEXTURE_MIN_FILTER,
					GL_LINEAR_MIPMAP_LINEAR );
			}

			void setNearest_nearestMipmap(  )
			{
				glTexParameteri(
					GL_TEXTURE_2D,
					GL_TEXTURE_MIN_FILTER,
					GL_NEAREST_MIPMAP_NEAREST );
			}

			void setLinear_nearestMipmap(  )
			{
				glTexParameteri(
					GL_TEXTURE_2D,
					GL_TEXTURE_MIN_FILTER,
					GL_NEAREST_MIPMAP_LINEAR );
			}

			void setNearest_linearMipmap(  )
			{
				glTexParameteri(
					GL_TEXTURE_2D,
					GL_TEXTURE_MIN_FILTER,
					GL_LINEAR_MIPMAP_NEAREST );
			}
		};
		struct MagFilter
		{
			void setLinear(  )
			{
				glTexParameteri(
					GL_TEXTURE_2D,
					GL_TEXTURE_MAG_FILTER,
					GL_LINEAR );
			}

			void setNearest(  )
			{
				glTexParameteri(
					GL_TEXTURE_2D,
					GL_TEXTURE_MAG_FILTER,
					GL_NEAREST );
			}
		};
	public:
		static WrapType wrapS;
		static WrapType wrapT;
		static MinFilter minF;
		static MagFilter magF;
	
	};
	myTexture::WrapType myTexture::wrapS( GL_TEXTURE_WRAP_S );
	myTexture::WrapType myTexture::wrapT( GL_TEXTURE_WRAP_T );
	myTexture::MinFilter myTexture::minF;
	myTexture::MagFilter myTexture::magF;
}

#endif
