#ifndef MYGPUOBJECTS_H
#define MYGPUOBJECTS_H

#include "glew_glfw_glm_soil.h"

namespace mogl {
	struct myVBO {
		GLuint id;
		unsigned N = 0;
		bool live = true;
		myVBO( unsigned _N = 1 ): N( _N ) {
			glGenBuffers( N, &(this->id) );
		}
		~myVBO(  ) {
			this->remove(  );
		}
		void bindTo( GLenum target ) {
			glBindBuffer( target, this->id ); // https://bit.ly/2PsLAr4
		}
		void remove(  ) {
			if ( live ) {
				glDeleteBuffers( N, &( this->id ) );
				live = false;
			}
		}
		template <typename T, size_t SizE>
		void loadToFor( T ( &data )[SizE], GLenum target, GLenum usage ) {
			this->bindTo( target );
			glBufferData( target, sizeof(data), data, usage );
		}
	};
	struct myVAO {
		GLuint id;
		unsigned N = 0;
		bool live = true;
		myVAO( unsigned _N = 1 ): N(_N) {
			glGenVertexArrays( N, &(this->id) );
		}
		~myVAO(  ) {
			this->remove(  );
		}
		void bind(  ) {
			glBindVertexArray( this->id );
		}
		void remove(  ) {
			if ( live ) {
				glDeleteVertexArrays( N, &( this->id ) );
				live = false;
			}
		}
		static void unbindAll(  ) {
			glBindVertexArray( 0 );
		}
	};
}

#endif
