#ifndef MOGL_H
#define MOGL_H



#include "myGlFunc.h"
#include "myShader.h"
#include "VertAttribPointer.h"
#include "myGPUobjects.h"
#include "myTexture.h"
#include "myDraw.h"
#include "myDefSettings.h"


#include "glew_glfw_glm_soil.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>




#endif
