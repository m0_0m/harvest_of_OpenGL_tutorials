#ifndef MYDRAW_H
#define MYDRAW_H

#include "myGPUobjects.h"

#include "glew_glfw_glm_soil.h"

namespace mogl {
	namespace draw {
		inline void arrays( GLenum mode, GLint first, GLsizei count ) {
		// >>> [ mode ] can be ( GL_POINTS, GL_LINE_STRIP, GL_LINE_LOOP, GL_LINES,
		// GL_LINE_STRIP_ADJACENCY, GL_LINES_ADJACENCY, GL_TRIANGLE_STRIP,
		// GL_TRIANGLE_FAN, GL_TRIANGLES, GL_TRIANGLE_STRIP_ADJACENCY,  GL_PATCHES,
		// or GL_TRIANGLES_ADJACENCY ).
		// >>> [ first ] is index of first vertex.
		// >>> [ count ] is quantity of vertices.
			glDrawArrays( mode, first, count );
		}
		inline void elements( myVBO& EBO, GLenum mode, GLsizei count ) {
			EBO.bindTo( GL_ELEMENT_ARRAY_BUFFER );
			glDrawElements( mode, count, GL_UNSIGNED_INT, 0 );
		}
	}
}

#endif
