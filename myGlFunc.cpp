#include"myGlFunc.h"
#include<iostream>



//glfwSetWindowShouldClose( window, GL_TRUE );

void mogl::myInitGLFW(  )
{
	glfwInit(  );
	glfwWindowHint( GLFW_CONTEXT_VERSION_MAJOR, 3 );
	glfwWindowHint( GLFW_CONTEXT_VERSION_MINOR, 3 );
	glfwWindowHint( GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE );
	// Изменение размера окна - выкл
	glfwWindowHint( GLFW_RESIZABLE, GL_FALSE );
}
bool mogl::getWindow( GLFWwindow*& window, int w, int h, const char str[] )
{
	mogl::myInitGLFW(  );
	window = glfwCreateWindow( w, h, str, nullptr, nullptr );
	if ( window == nullptr )
	{
		std::cout << "Can't create GLFW window" << std::endl;
		glfwTerminate(  );
		return false;
	}
	glfwMakeContextCurrent( window );
	glewExperimental = GL_TRUE;
	if ( glewInit(  ) != GLEW_OK )
	{
		std::cout << "Can't init GLEW" << std::endl;
		return false;
	}
	return true;
}
void mogl::view::setPort( GLFWwindow* window, int& w, int& h, bool needDepth )
{
	glfwGetFramebufferSize( window, &w, &h );
	glViewport( 0, 0, w, h );
	if ( needDepth ) glEnable( GL_DEPTH_TEST );
}
void mogl::setPolyFill( bool fill = true )
{
	glPolygonMode( GL_FRONT_AND_BACK, fill ? GL_FILL : GL_LINE );
}





void mogl::key_callback(
	GLFWwindow* window,
	int key,
	int scancode,
	int action,
	int mode )
{
	if ( key == GLFW_KEY_SPACE ) Keyboard::key::Space = action;
	if ( key == GLFW_KEY_ESCAPE )Keyboard::key::Esc = action;
	if ( key == GLFW_KEY_ENTER )Keyboard::key::Enter = action;
	if ( key == GLFW_KEY_RIGHT )Keyboard::key::Right = action;
	if ( key == GLFW_KEY_LEFT )Keyboard::key::Left = action;
	if ( key == GLFW_KEY_DOWN )Keyboard::key::Down = action;
	if ( key == GLFW_KEY_UP )Keyboard::key::Up = action;
	if ( key == GLFW_KEY_DELETE )Keyboard::key::Del = action;
	if ( key == GLFW_KEY_BACKSPACE )Keyboard::key::BackSpace = action;
	/*if ( key == GLFW_KEY_APOSTROPHE ) Keyboard::key::_APOSTROPHE = action;
	if ( key == GLFW_KEY_COMMA ) Keyboard::key::_COMMA = action;
	if ( key == GLFW_KEY_MINUS ) Keyboard::key::_MINUS = action;
	if ( key == GLFW_KEY_PERIOD ) Keyboard::key::_PERIOD = action;
	if ( key == GLFW_KEY_SLASH ) Keyboard::key::_SLASH = action;
	if ( key == GLFW_KEY_SEMICOLON ) Keyboard::key::_SEMICOLON = action;
	if ( key == GLFW_KEY_EQUAL ) Keyboard::key::_EQUAL = action;//*/
	if ( key == GLFW_KEY_0 ) Keyboard::key::_0 = action;
	if ( key == GLFW_KEY_1 ) Keyboard::key::_1 = action;
	if ( key == GLFW_KEY_2 ) Keyboard::key::_2 = action;
	if ( key == GLFW_KEY_3 ) Keyboard::key::_3 = action;
	if ( key == GLFW_KEY_4 ) Keyboard::key::_4 = action;
	if ( key == GLFW_KEY_5 ) Keyboard::key::_5 = action;
	if ( key == GLFW_KEY_6 ) Keyboard::key::_6 = action;
	if ( key == GLFW_KEY_7 ) Keyboard::key::_7 = action;
	if ( key == GLFW_KEY_8 ) Keyboard::key::_8 = action;
	if ( key == GLFW_KEY_9 ) Keyboard::key::_9 = action;//*/
	if ( key == GLFW_KEY_A ) Keyboard::key::A = action;
	if ( key == GLFW_KEY_B ) Keyboard::key::B = action;
	if ( key == GLFW_KEY_C ) Keyboard::key::C = action;
	if ( key == GLFW_KEY_D ) Keyboard::key::D = action;
	if ( key == GLFW_KEY_E ) Keyboard::key::E = action;
	if ( key == GLFW_KEY_F ) Keyboard::key::F = action;
	if ( key == GLFW_KEY_G ) Keyboard::key::G = action;
	if ( key == GLFW_KEY_H ) Keyboard::key::H = action;
	if ( key == GLFW_KEY_I ) Keyboard::key::I = action;
	if ( key == GLFW_KEY_J ) Keyboard::key::J = action;
	if ( key == GLFW_KEY_K ) Keyboard::key::K = action;
	if ( key == GLFW_KEY_L ) Keyboard::key::L = action;
	if ( key == GLFW_KEY_M ) Keyboard::key::M = action;
	if ( key == GLFW_KEY_N ) Keyboard::key::N = action;
	if ( key == GLFW_KEY_O ) Keyboard::key::O = action;
	if ( key == GLFW_KEY_P ) Keyboard::key::P = action;
	if ( key == GLFW_KEY_Q ) Keyboard::key::Q = action;
	if ( key == GLFW_KEY_R ) Keyboard::key::R = action;
	if ( key == GLFW_KEY_S ) Keyboard::key::S = action;
	if ( key == GLFW_KEY_T ) Keyboard::key::T = action;
	if ( key == GLFW_KEY_U ) Keyboard::key::U = action;
	if ( key == GLFW_KEY_V ) Keyboard::key::V = action;
	if ( key == GLFW_KEY_W ) Keyboard::key::W = action;
	if ( key == GLFW_KEY_X ) Keyboard::key::X = action;
	if ( key == GLFW_KEY_Y ) Keyboard::key::Y = action;
	if ( key == GLFW_KEY_Z ) Keyboard::key::Z = action;
	/*if ( key == GLFW_KEY_LEFT_BRACKET ) Keyboard::key::_LEFT_BRACKET = action;
	if ( key == GLFW_KEY_RIGHT_BRACKET ) Keyboard::key::_RIGHT_BRACKET = action;//*/
	/*if ( key == GLFW_KEY_BACKSLASH ) Keyboard::key::_BACKSLASH = action;
	if ( key == GLFW_KEY_GRAVE_ACCENT ) Keyboard::key::_GRAVE_ACCENT = action;
	if ( key == GLFW_KEY_WORLD_1 ) Keyboard::key::_WORLD_1 = action;
	if ( key == GLFW_KEY_WORLD_2 ) Keyboard::key::_WORLD_2 = action;//*/


	/*if ( key == GLFW_KEY_INSERT )Keyboard::key::_INSERT = action;
	if ( key == GLFW_KEY_HOME )Keyboard::key::_HOME = action;
	if ( key == GLFW_KEY_END )Keyboard::key::_END = action;//*/
	/*if ( key == GLFW_KEY_PAGE_UP )Keyboard::key::_PAGE_UP = action;
	if ( key == GLFW_KEY_PAGE_DOWN )Keyboard::key::_PAGE_DOWN = action;//*/
	/*if ( key == GLFW_KEY_TAB )Keyboard::key::_TAB = action;
	if ( key == GLFW_KEY_NUM_LOCK )Keyboard::key::_NUM_LOCK = action;
	if ( key == GLFW_KEY_CAPS_LOCK )Keyboard::key::_CAPS_LOCK = action;//*/
	/*if ( key == GLFW_KEY_PRINT_SCREEN )Keyboard::key::_PRINT_SCREEN = action;
	if ( key == GLFW_KEY_SCROLL_LOCK )Keyboard::key::_SCROLL_LOCK = action;
	if ( key == GLFW_KEY_PAUSE )Keyboard::key::_PAUSE = action;//*/
	/*if ( key == GLFW_KEY_F1 )Keyboard::key::_F1 = action;
	if ( key == GLFW_KEY_F2 )Keyboard::key::_F2 = action;
	if ( key == GLFW_KEY_F3 )Keyboard::key::_F3 = action;
	if ( key == GLFW_KEY_F4 )Keyboard::key::_F4 = action;
	if ( key == GLFW_KEY_F5 )Keyboard::key::_F5 = action;
	if ( key == GLFW_KEY_F6 )Keyboard::key::_F6 = action;
	if ( key == GLFW_KEY_F7 )Keyboard::key::_F7 = action;
	if ( key == GLFW_KEY_F8 )Keyboard::key::_F8 = action;
	if ( key == GLFW_KEY_F9 )Keyboard::key::_F9 = action;
	if ( key == GLFW_KEY_F10 )Keyboard::key::_F10 = action;
	if ( key == GLFW_KEY_F11 )Keyboard::key::_F11 = action;
	if ( key == GLFW_KEY_F12 )Keyboard::key::_F12 = action;//*/
	/*if ( key == GLFW_KEY_F13 )Keyboard::key::_F13 = action;
	if ( key == GLFW_KEY_F14 )Keyboard::key::_F14 = action;
	if ( key == GLFW_KEY_F15 )Keyboard::key::_F15 = action;
	if ( key == GLFW_KEY_F16 )Keyboard::key::_F16 = action;
	if ( key == GLFW_KEY_F17 )Keyboard::key::_F17 = action;
	if ( key == GLFW_KEY_F18 )Keyboard::key::_F18 = action;
	if ( key == GLFW_KEY_F19 )Keyboard::key::_F19 = action;
	if ( key == GLFW_KEY_F20 )Keyboard::key::_F20 = action;
	if ( key == GLFW_KEY_F21 )Keyboard::key::_F21 = action;
	if ( key == GLFW_KEY_F22 )Keyboard::key::_F22 = action;
	if ( key == GLFW_KEY_F23 )Keyboard::key::_F23 = action;
	if ( key == GLFW_KEY_F24 )Keyboard::key::_F24 = action;
	if ( key == GLFW_KEY_F25 )Keyboard::key::_F25 = action;//*/
	/*if ( key == GLFW_KEY_KP_0 )Keyboard::key::_KP_0 = action;
	if ( key == GLFW_KEY_KP_1 )Keyboard::key::_KP_1 = action;
	if ( key == GLFW_KEY_KP_2 )Keyboard::key::_KP_2 = action;
	if ( key == GLFW_KEY_KP_3 )Keyboard::key::_KP_3 = action;
	if ( key == GLFW_KEY_KP_4 )Keyboard::key::_KP_4 = action;
	if ( key == GLFW_KEY_KP_5 )Keyboard::key::_KP_5 = action;
	if ( key == GLFW_KEY_KP_6 )Keyboard::key::_KP_6 = action;
	if ( key == GLFW_KEY_KP_7 )Keyboard::key::_KP_7 = action;
	if ( key == GLFW_KEY_KP_8 )Keyboard::key::_KP_8 = action;
	if ( key == GLFW_KEY_KP_9 )Keyboard::key::_KP_9 = action;//*/
	/*if ( key == GLFW_KEY_KP_DECIMAL )Keyboard::key::_KP_DECIMAL = action;
	if ( key == GLFW_KEY_KP_DIVIDE )Keyboard::key::_KP_DIVIDE = action;
	if ( key == GLFW_KEY_KP_MULTIPLY )Keyboard::key::_KP_MULTIPLY = action;
	if ( key == GLFW_KEY_KP_SUBTRACT )Keyboard::key::_KP_SUBTRACT = action;
	if ( key == GLFW_KEY_KP_ADD )Keyboard::key::_KP_ADD = action;
	if ( key == GLFW_KEY_KP_ENTER )Keyboard::key::_KP_ENTER = action;
	if ( key == GLFW_KEY_KP_EQUAL )Keyboard::key::_KP_EQUAL = action;//*/
	if ( key == GLFW_KEY_LEFT_SHIFT )Keyboard::key::left::Shift = action;
	if ( key == GLFW_KEY_LEFT_CONTROL )Keyboard::key::left::Ctrl = action;
	if ( key == GLFW_KEY_LEFT_ALT )Keyboard::key::left::Alt = action;
	if ( key == GLFW_KEY_LEFT_SUPER )Keyboard::key::left::Sup = action;
	if ( key == GLFW_KEY_RIGHT_SHIFT )Keyboard::key::right::Shift = action;
	if ( key == GLFW_KEY_RIGHT_CONTROL )Keyboard::key::right::Ctrl = action;
	if ( key == GLFW_KEY_RIGHT_ALT )Keyboard::key::right::Alt = action;
	if ( key == GLFW_KEY_RIGHT_SUPER )Keyboard::key::right::Sup = action;
	if ( key == GLFW_KEY_MENU )Keyboard::key::MENU = action;
}


int mogl::Keyboard::key::Space = 0;
int mogl::Keyboard::key::Esc = 0;
int mogl::Keyboard::key::Enter = 0;
int mogl::Keyboard::key::Right = 0;
int mogl::Keyboard::key::Left = 0;
int mogl::Keyboard::key::Down = 0;
int mogl::Keyboard::key::Up = 0;
int mogl::Keyboard::key::Del = 0;
int mogl::Keyboard::key::BackSpace = 0;
//int mogl::Keyboard::key::_APOSTROPHE = 0;/* ' */
//int mogl::Keyboard::key::_COMMA = 0;/* , */
//int mogl::Keyboard::key::_MINUS = 0;/* - */
//int mogl::Keyboard::key::_PERIOD = 0;/* . */
//int mogl::Keyboard::key::_SLASH = 0;/* / */
//int mogl::Keyboard::key::_SEMICOLON = 0;/* ; */
//int mogl::Keyboard::key::_EQUAL = 0;/* = */
int mogl::Keyboard::key::_0 = 0;
int mogl::Keyboard::key::_1 = 0;
int mogl::Keyboard::key::_2 = 0;
int mogl::Keyboard::key::_3 = 0;
int mogl::Keyboard::key::_4 = 0;
int mogl::Keyboard::key::_5 = 0;
int mogl::Keyboard::key::_6 = 0;
int mogl::Keyboard::key::_7 = 0;
int mogl::Keyboard::key::_8 = 0;
int mogl::Keyboard::key::_9 = 0;
int mogl::Keyboard::key::A = 0;
int mogl::Keyboard::key::B = 0;
int mogl::Keyboard::key::C = 0;
int mogl::Keyboard::key::D = 0;
int mogl::Keyboard::key::E = 0;
int mogl::Keyboard::key::F = 0;
int mogl::Keyboard::key::G = 0;
int mogl::Keyboard::key::H = 0;
int mogl::Keyboard::key::I = 0;
int mogl::Keyboard::key::J = 0;
int mogl::Keyboard::key::K = 0;
int mogl::Keyboard::key::L = 0;
int mogl::Keyboard::key::M = 0;
int mogl::Keyboard::key::N = 0;
int mogl::Keyboard::key::O = 0;
int mogl::Keyboard::key::P = 0;
int mogl::Keyboard::key::Q = 0;
int mogl::Keyboard::key::R = 0;
int mogl::Keyboard::key::S = 0;
int mogl::Keyboard::key::T = 0;
int mogl::Keyboard::key::U = 0;
int mogl::Keyboard::key::V = 0;
int mogl::Keyboard::key::W = 0;
int mogl::Keyboard::key::X = 0;
int mogl::Keyboard::key::Y = 0;
int mogl::Keyboard::key::Z = 0;
//int mogl::Keyboard::key::_LEFT_BRACKET = 0;/* [ */
//int mogl::Keyboard::key::_RIGHT_BRACKET = 0;/* ] */
//int mogl::Keyboard::key::_BACKSLASH = 0;/* \ */
//int mogl::Keyboard::key::_GRAVE_ACCENT = 0;/* ` */
//int mogl::Keyboard::key::_WORLD_1 = 0;/* non-US #1 */
//int mogl::Keyboard::key::_WORLD_2 = 0;/* non-US #2 */

/*int mogl::Keyboard::key::_INSERT = 0;
int mogl::Keyboard::key::_HOME = 0;
int mogl::Keyboard::key::_END = 0;//*/
/*int mogl::Keyboard::key::_PAGE_UP = 0;
int mogl::Keyboard::key::_PAGE_DOWN = 0;//*/
/*int mogl::Keyboard::key::_TAB = 0;
int mogl::Keyboard::key::_NUM_LOCK = 0;
int mogl::Keyboard::key::_CAPS_LOCK = 0;//*/
/*int mogl::Keyboard::key::_PRstatic int_SCREEN = 0;
int mogl::Keyboard::key::_SCROLL_LOCK = 0;
int mogl::Keyboard::key::_PAUSE = 0;//*/
/*int mogl::Keyboard::key::_F1 = 0;
int mogl::Keyboard::key::_F2 = 0;
int mogl::Keyboard::key::_F3 = 0;
int mogl::Keyboard::key::_F4 = 0;
int mogl::Keyboard::key::_F5 = 0;
int mogl::Keyboard::key::_F6 = 0;
int mogl::Keyboard::key::_F7 = 0;
int mogl::Keyboard::key::_F8 = 0;
int mogl::Keyboard::key::_F9 = 0;
int mogl::Keyboard::key::_F10 = 0;
int mogl::Keyboard::key::_F11 = 0;
int mogl::Keyboard::key::_F12 = 0;//*/
/*int mogl::Keyboard::key::_F13 = 0;
int mogl::Keyboard::key::_F14 = 0;
int mogl::Keyboard::key::_F15 = 0;
int mogl::Keyboard::key::_F16 = 0;
int mogl::Keyboard::key::_F17 = 0;
int mogl::Keyboard::key::_F18 = 0;
int mogl::Keyboard::key::_F19 = 0;
int mogl::Keyboard::key::_F20 = 0;
int mogl::Keyboard::key::_F21 = 0;
int mogl::Keyboard::key::_F22 = 0;
int mogl::Keyboard::key::_F23 = 0;
int mogl::Keyboard::key::_F24 = 0;
int mogl::Keyboard::key::_F25 = 0;//*/
/*int mogl::Keyboard::key::_KP_0 = 0;
int mogl::Keyboard::key::_KP_1 = 0;
int mogl::Keyboard::key::_KP_2 = 0;
int mogl::Keyboard::key::_KP_3 = 0;
int mogl::Keyboard::key::_KP_4 = 0;
int mogl::Keyboard::key::_KP_5 = 0;
int mogl::Keyboard::key::_KP_6 = 0;
int mogl::Keyboard::key::_KP_7 = 0;
int mogl::Keyboard::key::_KP_8 = 0;
int mogl::Keyboard::key::_KP_9 = 0;//*/
/*int mogl::Keyboard::key::_KP_DECIMAL = 0;
int mogl::Keyboard::key::_KP_DIVIDE = 0;
int mogl::Keyboard::key::_KP_MULTIPLY = 0;
int mogl::Keyboard::key::_KP_SUBTRACT = 0;
int mogl::Keyboard::key::_KP_ADD = 0;
int mogl::Keyboard::key::_KP_ENTER = 0;
int mogl::Keyboard::key::_KP_EQUAL = 0;//*/
int mogl::Keyboard::key::left::Shift = 0;
int mogl::Keyboard::key::left::Ctrl = 0;
int mogl::Keyboard::key::left::Alt = 0;
int mogl::Keyboard::key::left::Sup = 0;
int mogl::Keyboard::key::right::Shift = 0;
int mogl::Keyboard::key::right::Ctrl = 0;
int mogl::Keyboard::key::right::Alt = 0;
int mogl::Keyboard::key::right::Sup = 0;
int mogl::Keyboard::key::MENU = 0;