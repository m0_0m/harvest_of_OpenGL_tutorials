#ifndef MYSHADER_H
#define MYSHADER_H

#include "glew_glfw_glm_soil.h"
#include <fstream>
#include <sstream>
#include <string>
#include <iostream>

namespace mogl {
	struct myShader {
		GLuint id;
		
		bool live = false;
		myShader( const char name[], GLenum shType ) {
			std::string shCode;
			std::ifstream in;
			in.exceptions( std::ifstream::badbit );
			try {
				in.open( name );
				std::stringstream inStream;
				inStream << in.rdbuf(  );
				in.close(  );
				shCode = inStream.str(  );
				live = true;
			} catch ( std::ifstream::failure e ) {
				std::cout << "ERROR::SHADER::FILE_NOT_SECCESFULLY_READ" << std::endl;
				live = false;
			}
			const GLchar* shaderCode = shCode.c_str(  );
			this->id = glCreateShader( shType );
			glShaderSource( this->id, 1, &shaderCode, NULL );
			glCompileShader( this->id );
		}
	
		void remove(  ) {
			if ( live ) {
			  	glDeleteShader( this->id );
			}
			live = false;
		}
	
		GLint is_ok(  ) {
			GLint success;
			GLchar infoLog[512];
			glGetShaderiv( this->id, GL_COMPILE_STATUS, &success );
			if ( !success ) {
				glGetShaderInfoLog( this->id, 512, NULL, infoLog );
				std::cout << "ERROR::SHADER::COMPILATION_FAILED\n"
					<< infoLog << std::endl;
			}
			return success;
		}
		~myShader(  ) {
			if ( live ) this->remove(  );
		}
	};
	
	struct myShProgram {
		GLuint id;
		bool live = false;
		myShProgram(  ) : live( true ) {
			this->id = glCreateProgram(  );
		}
		~myShProgram(  ) {
			this->remove(  );
		}
		void remove(  ) {
			if( live ) {
				glUseProgram( 0 );
				glDeleteProgram( this->id );
			}
			live = false;
		}
		void attachShader( GLuint shaderId ) {
			glAttachShader( this->id, shaderId );
		}
		void attachShader( myShader shader ) {
			glAttachShader( this->id, shader.id );
		}
		void linking() {
			glLinkProgram( this->id );
		}
		void use(  ) {
			if ( live ) {
				glUseProgram( this->id );
			}
		}
		bool is_ok(  ) {
			GLint success;
			GLchar infoLog[512];
			glGetProgramiv( this->id, GL_COMPILE_STATUS, &success );
			if ( !success ) {
				glGetProgramInfoLog( this->id, 512, NULL, infoLog );
				std::cout << "ERROR::PROGRAM::LINKING_FAILED\n"
					<< infoLog << std::endl;
				this->remove(  );
			}
			return success;
		}
		template<typename T> void uniSet( const char name[], T a0 ) {
			std::cout << "ERROR::myShader::uniSet1 \'Unknown type\'" << std::endl;
		}
		template<typename T> void uniSet( const char name[], T a0, T a1 ) {
			std::cout << "ERROR::myShader::uniSet2 \'Unknown type\'" << std::endl;
		}
		template<typename T> void uniSet( const char name[], T a0, T a1, T a2 ) {
			std::cout << "ERROR::myShader::uniSet3 \'Unknown type\'" << std::endl;
		}
		template<typename T> void uniSet( const char name[], T a0, T a1, T a2, T a3 ) {
			std::cout << "ERROR::myShader::uniSet4 \'Unknown type\'" << std::endl;
		}
		template<typename T> void uniSet1v( const char name[], unsigned count, T* ptr ) {
			std::cout << "ERROR::myShader::uniSet1[" << count << "] \'Unknown type\'" << std::endl;
		}
		template<typename T> void uniSet2v( const char name[], unsigned count, T* ptr ) {
			std::cout << "ERROR::myShader::uniSet2[" << count << "] \'Unknown type\'" << std::endl;
		}
		template<typename T> void uniSet3v( const char name[], unsigned count, T* ptr ) {
			std::cout << "ERROR::myShader::uniSet3[" << count << "] \'Unknown type\'" << std::endl;
		}
		template<typename T> void uniSet4v( const char name[], unsigned count, T* ptr ) {
			std::cout << "ERROR::myShader::uniSet4[" << count << "] \'Unknown type\'" << std::endl;
		}
		void uniSetM2x2v( const char name[], unsigned count, GLfloat* ptr, GLboolean transpose ) {
			GLint valLocation = glGetUniformLocation( this->id, name );
			glUniformMatrix2fv(valLocation,count,transpose,ptr);
		}
		void uniSetM3x3v( const char name[], unsigned count, GLfloat* ptr, GLboolean transpose ) {
			GLint valLocation = glGetUniformLocation( this->id, name );
			glUniformMatrix3fv(valLocation,count,transpose,ptr);
		}
		void uniSetM4x4v( const char name[], unsigned count, GLfloat* ptr, GLboolean transpose ) {
			GLint valLocation = glGetUniformLocation( this->id, name );
			glUniformMatrix4fv(valLocation,count,transpose,ptr);
		}
		void uniSetM2x3v( const char name[], unsigned count, GLfloat* ptr, GLboolean transpose ) {
			GLint valLocation = glGetUniformLocation( this->id, name );
			glUniformMatrix2x3fv(valLocation,count,transpose,ptr);
		}
		void uniSetM3x2v( const char name[], unsigned count, GLfloat* ptr, GLboolean transpose ) {
			GLint valLocation = glGetUniformLocation( this->id, name );
			glUniformMatrix3x2fv(valLocation,count,transpose,ptr);
		}
		void uniSetM2x4v( const char name[], unsigned count, GLfloat* ptr, GLboolean transpose ) {
			GLint valLocation = glGetUniformLocation( this->id, name );
			glUniformMatrix2x4fv(valLocation,count,transpose,ptr);
		}
		void uniSetM4x2v( const char name[], unsigned count, GLfloat* ptr, GLboolean transpose ) {
			GLint valLocation = glGetUniformLocation( this->id, name );
			glUniformMatrix4x2fv(valLocation,count,transpose,ptr);
		}
		void uniSetM3x4v( const char name[], unsigned count, GLfloat* ptr, GLboolean transpose ) {
			GLint valLocation = glGetUniformLocation( this->id, name );
			glUniformMatrix3x4fv(valLocation,count,transpose,ptr);
		}
		void uniSetM4x3v( const char name[], unsigned count, GLfloat* ptr, GLboolean transpose ) {
			GLint valLocation = glGetUniformLocation( this->id, name );
			glUniformMatrix4x3fv(valLocation,count,transpose,ptr);
		}
	};
	//------------GLSL float
	template<> void myShProgram::uniSet<GLfloat>( const char name[], GLfloat a0 ) {
		GLint valLocation = glGetUniformLocation( this->id, name );
		glUniform1f( valLocation, a0 );
	}
	template<> void myShProgram::uniSet<GLfloat>( const char name[], GLfloat a0, GLfloat a1 ) {
		GLint valLocation = glGetUniformLocation( this->id, name );
		glUniform2f( valLocation, a0, a1 );
	}
	template<> void myShProgram::uniSet<GLfloat>( const char name[], GLfloat a0, GLfloat a1, GLfloat a2 ) {
		GLint valLocation = glGetUniformLocation( this->id, name );
		glUniform3f( valLocation, a0, a1, a2 );
	}
	template<> void myShProgram::uniSet<GLfloat>( const char name[], GLfloat a0, GLfloat a1, GLfloat a2, GLfloat a3 ) {
		GLint valLocation = glGetUniformLocation( this->id, name );
		glUniform4f( valLocation, a0, a1, a2, a3 );
	}
	template<> void myShProgram::uniSet1v<GLfloat>( const char name[], unsigned count, GLfloat* ptr ) {
		GLint valLocation = glGetUniformLocation( this->id, name );
		glUniform1fv(valLocation,count,ptr);
	}
	template<> void myShProgram::uniSet2v<GLfloat>( const char name[], unsigned count, GLfloat* ptr ) {
		GLint valLocation = glGetUniformLocation( this->id, name );
		glUniform2fv(valLocation,count,ptr);
	}
	template<> void myShProgram::uniSet3v<GLfloat>( const char name[], unsigned count, GLfloat* ptr ) {
		GLint valLocation = glGetUniformLocation( this->id, name );
		glUniform3fv(valLocation,count,ptr);
	}
	template<> void myShProgram::uniSet4v<GLfloat>( const char name[], unsigned count, GLfloat* ptr ) {
		GLint valLocation = glGetUniformLocation( this->id, name );
		glUniform4fv(valLocation,count,ptr);
	}
	//-----------GLSL int
	template<> void myShProgram::uniSet<GLint>( const char name[], GLint a0 ) {
		GLint valLocation = glGetUniformLocation( this->id, name );
		glUniform1i( valLocation, a0 );
	}
	template<> void myShProgram::uniSet<GLint>( const char name[], GLint a0, GLint a1 ) {
		GLint valLocation = glGetUniformLocation( this->id, name );
		glUniform2i( valLocation, a0, a1 );
	}
	template<> void myShProgram::uniSet<GLint>( const char name[], GLint a0, GLint a1, GLint a2 ) {
		GLint valLocation = glGetUniformLocation( this->id, name );
		glUniform3i( valLocation, a0, a1, a2 );
	}
	template<> void myShProgram::uniSet<GLint>( const char name[], GLint a0, GLint a1, GLint a2, GLint a3 ) {
		GLint valLocation = glGetUniformLocation( this->id, name );
		glUniform4i( valLocation, a0, a1, a2, a3 );
	}
	template<> void myShProgram::uniSet1v<GLint>( const char name[], unsigned count, GLint* ptr ) {
		GLint valLocation = glGetUniformLocation( this->id, name );
		glUniform1iv(valLocation,count,ptr);
	}
	template<> void myShProgram::uniSet2v<GLint>( const char name[], unsigned count, GLint* ptr ) {
		GLint valLocation = glGetUniformLocation( this->id, name );
		glUniform2iv(valLocation,count,ptr);
	}
	template<> void myShProgram::uniSet3v<GLint>( const char name[], unsigned count, GLint* ptr ) {
		GLint valLocation = glGetUniformLocation( this->id, name );
		glUniform3iv(valLocation,count,ptr);
	}
	template<> void myShProgram::uniSet4v<GLint>( const char name[], unsigned count, GLint* ptr ) {
		GLint valLocation = glGetUniformLocation( this->id, name );
		glUniform4iv(valLocation,count,ptr);
	}
	//-----------GLSL unsigned int
	template<> void myShProgram::uniSet<GLuint>( const char name[], GLuint a0 ) {
		GLint valLocation = glGetUniformLocation( this->id, name );
		glUniform1ui( valLocation, a0 );
	}
	template<> void myShProgram::uniSet<GLuint>( const char name[], GLuint a0, GLuint a1 ) {
		GLint valLocation = glGetUniformLocation( this->id, name );
		glUniform2ui( valLocation, a0, a1 );
	}
	template<> void myShProgram::uniSet<GLuint>( const char name[], GLuint a0, GLuint a1, GLuint a2 ) {
		GLint valLocation = glGetUniformLocation( this->id, name );
		glUniform3ui( valLocation, a0, a1, a2 );
	}
	template<> void myShProgram::uniSet<GLuint>( const char name[], GLuint a0, GLuint a1, GLuint a2, GLuint a3 ) {
		GLint valLocation = glGetUniformLocation( this->id, name );
		glUniform4ui( valLocation, a0, a1, a2, a3 );
	}
	template<> void myShProgram::uniSet1v<GLuint>( const char name[], unsigned count, GLuint* ptr ) {
		GLint valLocation = glGetUniformLocation( this->id, name );
		glUniform1uiv(valLocation,count,ptr);
	}
	template<> void myShProgram::uniSet2v<GLuint>( const char name[], unsigned count, GLuint* ptr ) {
		GLint valLocation = glGetUniformLocation( this->id, name );
		glUniform2uiv(valLocation,count,ptr);
	}
	template<> void myShProgram::uniSet3v<GLuint>( const char name[], unsigned count, GLuint* ptr ) {
		GLint valLocation = glGetUniformLocation( this->id, name );
		glUniform3uiv(valLocation,count,ptr);
	}
	template<> void myShProgram::uniSet4v<GLuint>( const char name[], unsigned count, GLuint* ptr ) {
		GLint valLocation = glGetUniformLocation( this->id, name );
		glUniform4uiv(valLocation,count,ptr);
	}
}

#endif
