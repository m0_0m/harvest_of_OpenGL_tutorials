#version 330 core

in vec2 texPos;
uniform sampler2D ourTexture1;
uniform sampler2D ourTexture2;

out vec4 color;

void main()
{
    color = mix( texture( ourTexture1, texPos ), texture( ourTexture2, texPos ), 0.4f );
}
