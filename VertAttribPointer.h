#ifndef MYVERTATTRIBPOINTER_H
#define MYVERTATTRIBPOINTER_H

#include "glew_glfw_glm_soil.h"
#include <iostream>

template <class T> class vertAttribPtr {
public:
	static void set( int posGlslVal, int arrSizeGlslVal, bool needNorm, int step, long startPoint ) {
		std::cout << "vertAttribPtr::set(  ) ERROR unexpected type" << std::endl;
	}
private:
	vertAttribPtr(  ) {}
	~vertAttribPtr(  ) {}
};

template <> void vertAttribPtr<GLfloat>::set( int posGlslVal, int arrSizeGlslVal, bool needNorm, int step, long startPoint ) {
	glVertexAttribPointer( posGlslVal, arrSizeGlslVal, GL_FLOAT
			, ( needNorm ? GL_TRUE : GL_FALSE )
			, step * sizeof(GLfloat), (GLvoid*)(startPoint * sizeof(GLfloat))  );
	glEnableVertexAttribArray( posGlslVal );
}

template <> void vertAttribPtr<GLdouble>::set( int posGlslVal, int arrSizeGlslVal, bool needNorm, int step, long startPoint ) {
	glVertexAttribPointer( posGlslVal, arrSizeGlslVal, GL_DOUBLE
			, ( needNorm ? GL_TRUE : GL_FALSE )
			, step * sizeof(GLdouble), (GLvoid*)(startPoint * sizeof(GLdouble))  );
	glEnableVertexAttribArray( posGlslVal );
}

template <> void vertAttribPtr<GLbyte>::set( int posGlslVal, int arrSizeGlslVal, bool needNorm, int step, long startPoint ) {
	glVertexAttribPointer( posGlslVal, arrSizeGlslVal, GL_BYTE
			, ( needNorm ? GL_TRUE : GL_FALSE )
			, step * sizeof(GLbyte), (GLvoid*)(startPoint * sizeof(GLbyte))  );
	glEnableVertexAttribArray( posGlslVal );
}

template <> void vertAttribPtr<GLubyte>::set( int posGlslVal, int arrSizeGlslVal, bool needNorm, int step, long startPoint ) {
	glVertexAttribPointer( posGlslVal, arrSizeGlslVal, GL_UNSIGNED_BYTE
			, ( needNorm ? GL_TRUE : GL_FALSE )
			, step * sizeof(GLubyte), (GLvoid*)(startPoint * sizeof(GLubyte))  );
	glEnableVertexAttribArray( posGlslVal );
}

template <> void vertAttribPtr<GLshort>::set( int posGlslVal, int arrSizeGlslVal, bool needNorm, int step, long startPoint ) {
	glVertexAttribPointer( posGlslVal, arrSizeGlslVal, GL_SHORT
			, ( needNorm ? GL_TRUE : GL_FALSE )
			, step * sizeof(GLshort), (GLvoid*)(startPoint * sizeof(GLshort)) );
	glEnableVertexAttribArray( posGlslVal );
}

template <> void vertAttribPtr<GLushort>::set( int posGlslVal, int arrSizeGlslVal, bool needNorm, int step, long startPoint ) {
	glVertexAttribPointer( posGlslVal, arrSizeGlslVal, GL_UNSIGNED_SHORT
			, ( needNorm ? GL_TRUE : GL_FALSE )
			, step * sizeof(GLushort), (GLvoid*)(startPoint * sizeof(GLushort)) );
	glEnableVertexAttribArray( posGlslVal );
}

template <> void vertAttribPtr<GLint>::set( int posGlslVal, int arrSizeGlslVal, bool needNorm, int step, long startPoint ) {
	glVertexAttribPointer( posGlslVal, arrSizeGlslVal, GL_INT
			, ( needNorm ? GL_TRUE : GL_FALSE )
			, step * sizeof(GLint), (GLvoid*)(startPoint * sizeof(GLint))  );
	glEnableVertexAttribArray( posGlslVal );
}

template <> void vertAttribPtr<GLuint>::set( int posGlslVal, int arrSizeGlslVal, bool needNorm, int step, long startPoint ) {
	glVertexAttribPointer( posGlslVal, arrSizeGlslVal, GL_UNSIGNED_INT
			, ( needNorm ? GL_TRUE : GL_FALSE )
			, step * sizeof(GLuint), (GLvoid*)(startPoint * sizeof(GLuint)) );
	glEnableVertexAttribArray( posGlslVal );
}

#endif
