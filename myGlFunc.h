#ifndef MYGLFUNC_H
#define MYGLFUNC_H

#include "glew_glfw_glm_soil.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace mogl
{

	void myInitGLFW(  );
	
	bool getWindow( GLFWwindow*& window, int w, int h, const char str[] );
	
	namespace view
	{
		void setPort( GLFWwindow* window, int& w, int& h, bool needDepth );
		inline glm::mat4 setOrtho( float width, float height, float depth )
		{
			return glm::ortho( 0.0f, width, 0.0f, height, 0.1f, depth );
		}

		inline glm::mat4 set3D( float width, float height, float depth )
		{
			return glm::transpose( glm::perspective(
												glm::radians( 45.0f ),
												width / height,
												0.1f,
												depth ) );
		}
	}

	void setPolyFill( bool fill );

	namespace Keyboard {
		class key {
		public:
			static int Space;
			static int Esc;
			static int Enter;
			static int Right;
			static int Left;
			static int Down;
			static int Up;
			static int Del;
			static int BackSpace;
			//static int _APOSTROPHE;/* ' */
			//static int _COMMA;/* , */
			//static int _MINUS;/* - */
			//static int _PERIOD;/* . */
			//static int _SLASH;/* / */
			//static int _SEMICOLON;/* ; */
			//static int _EQUAL;/* = */
			static int _0;
			static int _1;
			static int _2;
			static int _3;
			static int _4;
			static int _5;
			static int _6;
			static int _7;
			static int _8;
			static int _9;
			static int A;
			static int B;
			static int C;
			static int D;
			static int E;
			static int F;
			static int G;
			static int H;
			static int I;
			static int J;
			static int K;
			static int L;
			static int M;
			static int N;
			static int O;
			static int P;
			static int Q;
			static int R;
			static int S;
			static int T;
			static int U;
			static int V;
			static int W;
			static int X;
			static int Y;
			static int Z;
			//static int _LEFT_BRACKET;/* [ */
			//static int _RIGHT_BRACKET;/* ] */
			//static int _BACKSLASH;/* \ */
			//static int _GRAVE_ACCENT;/* ` */
			//static int _WORLD_1;/* non-US #1 */
			//static int _WORLD_2;/* non-US #2 */

			/*static int _INSERT;
			static int _HOME;
			static int _END;//*/
			/*static int _PAGE_UP;
			static int _PAGE_DOWN;//*/
			/*static int _TAB;
			static int _NUM_LOCK;
			static int _CAPS_LOCK;//*/
			/*static int _PRstatic int_SCREEN;
			static int _SCROLL_LOCK;
			static int _PAUSE;//*/
			/*static int _F1;
			static int _F2;
			static int _F3;
			static int _F4;
			static int _F5;
			static int _F6;
			static int _F7;
			static int _F8;
			static int _F9;
			static int _F10;
			static int _F11;
			static int _F12;//*/
			/*static int _F13;
			static int _F14;
			static int _F15;
			static int _F16;
			static int _F17;
			static int _F18;
			static int _F19;
			static int _F20;
			static int _F21;
			static int _F22;
			static int _F23;
			static int _F24;
			static int _F25;//*/
			/*static int _KP_0;
			static int _KP_1;
			static int _KP_2;
			static int _KP_3;
			static int _KP_4;
			static int _KP_5;
			static int _KP_6;
			static int _KP_7;
			static int _KP_8;
			static int _KP_9;//*/
			/*static int _KP_DECIMAL;
			static int _KP_DIVIDE;
			static int _KP_MULTIPLY;
			static int _KP_SUBTRACT;
			static int _KP_ADD;
			static int _KP_ENTER;
			static int _KP_EQUAL;//*/
			class left {
			public:
				static int Shift;
				static int Ctrl;
				static int Alt;
				static int Sup;
			};
			class right {
			public:
				static int Shift;
				static int Ctrl;
				static int Alt;
				static int Sup;
			};
			static int MENU;
		};
	}
	void key_callback(
		GLFWwindow* window,
		int key,
		int scancode,
		int action,
		int mode );
}

#endif
