#ifndef MYDEFSETTINGS_H
#define MYDEFSETTINGS_H

#include "myTexture.h"

#include "glew_glfw_glm_soil.h"

namespace mogl {
	inline void defTextureSettings(  ) {
		myTexture::wrapS.repeat(  );
		myTexture::wrapT.repeat(  );
		myTexture::minF.setLinear_linearMipmap(  );
		myTexture::magF.setLinear(  );
	}
	inline bool defShProgram( myShProgram& pr, const char* v, const char* f ) {
		myShader shVert( v, GL_VERTEX_SHADER );
		if ( !shVert.is_ok(  ) ) {
			std::cout << "VERTEX_SHADER" << std::endl;
			glfwTerminate(  );
			return true;
		}
		myShader shFrag( f, GL_FRAGMENT_SHADER );
		if ( !shFrag.is_ok(  ) ) {
			std::cout << "FRAGMENT_SHADER" << std::endl;
			glfwTerminate(  );
			return true;
		}
		pr.attachShader( shVert );
		pr.attachShader( shFrag );
		pr.linking(  );
		if ( !pr.is_ok() ) {
			std::cout << "PROGRAM_SHADER" << std::endl;
			glfwTerminate(  );
			return true;
		}
		return false;
	}
}


#endif
