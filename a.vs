#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 myTexPos;

out vec2 texPos;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
	gl_Position = transpose(projection) * view * model * vec4(position, 1.0f);
	texPos = vec2( myTexPos.x, 1.0f - myTexPos.y );
}
